lock "3.6.0"

ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :linked_files, fetch(:linked_files, []).push("config/database.yml", "config/secrets.yml", "config/cable.yml")
set :linked_dirs, fetch(:linked_dirs, []).push("log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system")

set :passenger_restart_with_touch, true
