require 'test_helper'

class CommitFetcherJobTest < ActiveJob::TestCase
  setup do
    @project = Project.create(name: "test")
  end

  test "fetching the commits is queued after we create a new deploy" do
    assert_enqueued_with(job: CommitFetcherJob) do
      @project.deploys.create(revision: "abc")
    end
  end
end
