require 'test_helper'

class DeployTest < ActiveSupport::TestCase
  setup do
    @project = Project.create(name: "test")
  end

  test "revision must be present" do
    deploy = Deploy.new(revision: nil, project: @project)

    assert !deploy.valid?
  end

  test "belongs to project" do
    deploy = Deploy.new(revision: nil, project: @project)

    assert deploy.project == @project
  end

  test "deletion of commits when deleted" do
    deploy = @project.deploys.create!(revision: "abc")
    3.times do |i|
      deploy.commits.create!(
        sha: "abc#{i}",
        short_sha: i,
        author_name: "Joren",
        author_email: "git@jorendegroof.be",
        title: "Changes #{i}"
      )
    end

    deploy.destroy

    assert Commit.count == 0
  end

  test "previous_sha when first deploy" do
    deploy = @project.deploys.create!(revision: "abc")

    assert deploy.previous_sha == nil
  end

  test "previous_sha when a lot of commits" do
    oldest = @project.deploys.create!(revision: "oldest", created_at: 3.week.ago)
    old = @project.deploys.create!(revision: "old", created_at: 2.week.ago)
    newer = @project.deploys.create!(revision: "new", created_at: 1.week.ago)
    newest = @project.deploys.create!(revision: "abc")

    assert newest.previous_sha == "new"
    assert newer.previous_sha == "old"
  end

  test "previous_sha scoped per environment" do
    @project.deploys.create!(environment: "production", revision: "prod_one", created_at: 1.day.ago)
    @project.deploys.create!(environment: "staging", revision: "stag_one", created_at: 1.day.ago)
    production = @project.deploys.create!(environment: "production", revision: "prod_two")
    staging = @project.deploys.create!(environment: "staging", revision: "stag_two")

    assert production.previous_sha == "prod_one"
    assert staging.previous_sha == "stag_one"
  end
end
