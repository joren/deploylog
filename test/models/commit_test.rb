require 'test_helper'

class CommitTest < ActiveSupport::TestCase
  test "validations" do
    commit = Commit.new

    assert_not commit.valid?
    assert commit.errors.messages[:sha].include?("can't be blank")
    assert commit.errors.messages[:short_sha].include?("can't be blank")
    assert commit.errors.messages[:author_name].include?("can't be blank")
    assert commit.errors.messages[:author_email].include?("can't be blank")
    assert commit.errors.messages[:title].include?("can't be blank")
  end
end
