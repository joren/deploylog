require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test "name presence validation" do
    project = Project.new(name: "test")

    assert project.valid?
  end

  test "invalid empy name" do
    project = Project.new(name: nil)

    assert !project.valid?
  end

  test "has many deploys" do
    project = Project.create!(name: "test")
    3.times do |i|
      project.deploys.create(revision: i.to_s)
    end

    assert project.deploys.count == 3
  end

  test "deletion of deploys when deleted" do
    project = Project.create!(name: "test")
    3.times do |i|
      project.deploys.create(revision: i.to_s)
    end
    project.destroy

    assert Deploy.count == 0
  end

  test "gets an api key on create" do
    project = Project.create!(name: "test")

    assert project.api_key.present?, "Project api_key is present"
  end

  test "gitlab_id with namespaced project" do
    project = Project.new(repo: "https://gitlab.openminds.be/dev/sock")

    assert project.gitlab_id == "dev%2Fsock"
  end

  test "gitlab_id with normal url" do
    project = Project.new(repo: "https://gitlab.openminds.be/awesome_app")

    assert project.gitlab_id == "awesome_app"
  end

  test "gitlab_url" do
    project = Project.new(repo: "https://gitlab.openminds.be/foobar")

    assert project.gitlab_url == "https://gitlab.openminds.be"
  end
end
