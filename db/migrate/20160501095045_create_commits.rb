class CreateCommits < ActiveRecord::Migration[5.0]
  def change
    create_table :commits do |t|
      t.belongs_to :deploy, foreign_key: true
      t.string :sha
      t.string :author_name
      t.string :author_email
      t.string :short_sha
      t.string :title

      t.timestamps
    end
  end
end
