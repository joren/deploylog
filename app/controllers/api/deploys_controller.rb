class Api::DeploysController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :user_required!

  before_action :fetch_project

  def create
    deploy = @project.deploys.new(
      revision: params[:revision],
      deployer: params[:deployer],
      environment: params[:environment],
      location: params[:location]
    )
    if deploy.save
      render json: { message: "Successfully logged deploy" }, status: 200
    else
      render json: deploy.errors.messages.to_json, status: 500
    end
  end

private

  def fetch_project
    @project ||= Project.find_by(api_key: params[:project_key])
    unless @project.present?
      return render(json: { message: "Project not found" }, status: 404)
    end
  end
end
