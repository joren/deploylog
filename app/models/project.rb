class Project < ApplicationRecord
  paginates_per 20

  has_many :deploys, dependent: :destroy
  has_many :commits, through: :deploys

  validates :name, presence: true

  before_create :generate_api

  scope :search, ->(query) { where("name LIKE ?", "%#{query}") }

  def gitlab_id
    # We have to encode the slash between the namespace and the project
    # do be able to use this in the api.
    URI.parse(repo).path.sub(/^\//, "").gsub(/\//, "%2F")
  end

  def gitlab_url
    parsed_uri = URI.parse(repo)
    [parsed_uri.scheme, "://", parsed_uri.host].join
  end

private

  def generate_api
    token = SecureRandom.urlsafe_base64(20).tr('lIO0', 'sxyz')
    while(Project.exists?(api_key: token)) do
      token = SecureRandom.urlsafe_base64(20).tr('lIO0', 'sxyz')
    end
    self.api_key = token
  end
end
