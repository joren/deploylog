class CommitFetcherJob < ApplicationJob
  queue_as :default

  def perform(deploy)
    deploy.fetch_commits
  end
end
